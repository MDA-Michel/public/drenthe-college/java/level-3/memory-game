package classes;

import javax.swing.*;
import java.util.ArrayList;

public class Board {

    public JFrame frame;

    // Opentiles: this ArrayList will contain all Tile classes which have been clicked on.
    ArrayList<Integer> openTiles = new ArrayList<Integer>();

    // Board(): draws the board, makes it visible after tiles have been added in the 'Game' class.
    public Board(String name){
        this.frame = new JFrame(name);
        this.frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.frame.setSize(300,325);  
        this.frame.setLayout(null);  
        this.frame.setVisible(false);
    }

    // setTiles(): Sets the tiles on the board (frame) - placement x/y axes have already been taken care off in the Tile class
    public void setTiles(JButton button){
        this.frame.add(button);
    }

    // compareTiles(): compares the first 2 tiles clicked. 
    public void compareTiles(Tile tiles[]){
        for (int tile = 0; tile < tiles.length; tile++) {
            if (this.openTiles.size() == 3) {
                if (tiles[this.openTiles.get(0)].getValue() == tiles[this.openTiles.get(1)].getValue()) {
                    System.out.println("Matched!");
                    tiles[this.openTiles.get(0)].setMatchedState();
                    tiles[this.openTiles.get(1)].setMatchedState();
                    tiles[this.openTiles.get(2)].turnToBack();
                } else {
                    System.out.println("No match!");
                    tiles[this.openTiles.get(0)].turnToBack();
                    tiles[this.openTiles.get(1)].turnToBack();
                }
                tiles[this.openTiles.get(0)].setGuessedState(false);
                tiles[this.openTiles.get(1)].setGuessedState(false);
                tiles[this.openTiles.get(2)].turnToBack();
                this.openTiles.clear();
            }

            if (tiles[tile].getCurrentState() && !tiles[tile].getGuessedState() && !tiles[tile].getMatchedState()) {
                this.openTiles.add(tile);
                tiles[tile].setGuessedState(true);
            }
        }
    }
}