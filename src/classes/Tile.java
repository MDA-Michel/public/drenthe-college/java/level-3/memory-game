package classes;

import javax.swing.JButton; 

public class Tile {
    private String value;
    private int hiddenValue;
    private int id;
    private JButton tile;
    private int x;
    private int y;
    private int width = 60;
    private int height = 60;
    private boolean open;
    private boolean guessed;
    private boolean matched;

    // Tile(): constructor instantiates A value it will show, an hiddenValue and an associated ID
    public Tile (int id, int value){
        this.value = "X";
        this.hiddenValue = value;
        this.id = id;
        setTile();
    }

    // getId(): returns the id of the tile
    public int getId(){
        return this.id;
    }

    // getValue(): returns the hidden value of the button (int between 1 - 8)
    public int getValue(){
        return this.hiddenValue;
    }

    // getTile(): returns the JButton tile
    public JButton getTile(){
        return this.tile;
    }
        
    // setTile(): set tile values, x/y asses & width/height.
    public void setTile(){
        this.tile = new JButton(this.value);
        if (this.id <= 4) {
            y = 10;
            x = 10 + (70 * (this.id -1));
        } else if (this.id <= 8) {
            y = 80;
            x = 10 + (70 * (this.id -5));
        } else if (this.id <= 12) {
            y = 150;
            x = 10 + (70 * (this.id -9));
        } else if (this.id <= 16) {
            y = 220;
            x = 10 + (70 * (this.id -13));
        }
        this.tile.setBounds(x, y, width, height);
        this.open = false;
        this.guessed = false;
    }

    // turnToFront(): turns the tile
    public void turnToFront(){
        this.open = true;
        this.value = Integer.toString(this.hiddenValue);
        getTile().setText(this.value);
        getTile().setEnabled(false);
    }

    // turnToBack(): turns the tile back to the backside
    public void turnToBack(){
        this.open = false;
        this.guessed = false;
        this.value = "X";
        getTile().setText(this.value);
        getTile().setEnabled(true);
    }

    // setCurrentState(): sets the current state of the tile to false
    public void setCurrentState(){
        this.open = false;
    }

    // setGuessedState(): sets the guessed state of tile tile to value (true/false)
    public void setGuessedState(boolean value) {
        this.guessed = value;
    }

    // setMatchedState(): if 2 tiles match, it sets both the tiles to the state 'matched'
    public void setMatchedState(){
        this.matched = true;
    }

    // getCurrentState(): returns if the tile is open or closed
    public boolean getCurrentState(){
        return this.open;
    }

    // getGuessedState(): returns the guessed state 
    public boolean getGuessedState(){
        return this.guessed;
    }

    // getMatchedState(): returns the matched state
    public boolean getMatchedState(){
        return this.matched;
    }
}
