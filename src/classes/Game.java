package classes;

import java.util.*;
import java.awt.event.*;  

public class Game {
    public Game(Board board){
        // ArrayList that is being made & shuffled
        ArrayList<Integer> numbers = new ArrayList<>(Arrays. asList(1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8));
        Collections.shuffle(numbers);

        // 16 spots for the type 'Tile' are being made, in the loop underneath these classes are being instantiated;
        Tile Tiles[] = new Tile[16];

        // created 16 tile classes
        for (int id = 1; id < Tiles.length+1; id++) {
            // generate the tiles, set the attributes/properties of the tiles;
            Tiles[id-1] = new Tile(id, numbers.get(id-1));
            board.setTiles(Tiles[id-1].getTile());
        }
        board.frame.setVisible(true);

        // set the action listener for all tiles
        for (int tile = 0; tile < Tiles.length; tile++) {
            final int id = tile;
            Tiles[tile].getTile().addActionListener(new ActionListener(){  
                public void actionPerformed(ActionEvent e){
                        if (!Tiles[id].getCurrentState()){
                            Tiles[id].turnToFront();
                            board.compareTiles(Tiles);
                        }
                    }
                }
            );
        }
    }
}
